// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

contract ZombieFactory {

    event NewZombie(uint zombieId, string name, uint dna);

    uint dnaDigits = 16;
    uint dnaModulus = 10 ** dnaDigits;

    struct Zombie {
        string name;
        uint dna;
    }

    Zombie[] public zombies;

    mapping (uint => address) public zombieToOwner;
    mapping (address => uint) ownerZombieCount;

    function createZombie(string memory _name, uint _dna) public {
        uint id = zombies.length;
        zombies.push(Zombie(_name, _dna));
        zombieToOwner[id] = msg.sender;
        ownerZombieCount[msg.sender]++;
        emit NewZombie(id, _name, _dna);
    }

    function generateRandomDna(string memory _str) public view returns (uint) {
        uint rand = uint(keccak256(abi.encodePacked(_str)));
        return rand % dnaModulus;
    }

    function createRandomZombie(string memory _name) public {
        require(0 == ownerZombieCount[msg.sender]);
        uint randDna = generateRandomDna(_name);
        createZombie(_name, randDna);
    }

}